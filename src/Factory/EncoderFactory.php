<?php


namespace Vallarj\Laminas\JsonApi\Factory;


use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Vallarj\JsonApi\Encoder;
use Vallarj\JsonApi\SchemaManager;

class EncoderFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return Encoder::create($container->get(SchemaManager::class));
    }
}