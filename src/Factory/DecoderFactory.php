<?php


namespace Vallarj\Laminas\JsonApi\Factory;


use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Vallarj\JsonApi\Decoder;
use Vallarj\JsonApi\SchemaManager;

class DecoderFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return Decoder::create($container->get(SchemaManager::class));
    }
}