<?php


namespace Vallarj\Laminas\JsonApi\Factory;


use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Vallarj\JsonApi\SchemaManager;

class SchemaManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $schemaManager = new SchemaManager();

        $config = $container->get('config');
        $libConfig = $config['php-json-api'];

        $schemas = $libConfig['schemas'];
        foreach($schemas as $schema) {
            $schemaManager->register($container->get($schema), $schema);
        }

        return $schemaManager;
    }
}
