<?php


namespace Vallarj\Laminas\JsonApi;


class Module
{
    public function getConfig(): array
    {
        return (new ConfigProvider())->getModuleConfiguration();
    }
}