<?php


namespace Vallarj\Laminas\JsonApi;


use Vallarj\JsonApi\Decoder;
use Vallarj\JsonApi\Encoder;
use Vallarj\JsonApi\SchemaManager;

class ConfigProvider
{
    /**
     * Retrieve configuration for laminas-json-api.
     *
     * @return array
     */
    public function __invoke(): array
    {
        return [
            'php-json-api' => $this->getJsonApiConfiguration(),
            'dependencies' => $this->getDependencies()
        ];
    }

    /**
     * Returns the module configuration for laminas-mvc projects
     *
     * @return array
     */
    public function getModuleConfiguration(): array
    {
        return [
            'php-json-api' => $this->getJsonApiConfiguration(),
            'service_manager' => $this->getDependencies()
        ];
    }

    /**
     * Returns the default object mapper configuration
     *
     * @return array
     */
    public function getJsonApiConfiguration(): array
    {
        return [
            'schemas' => [],
        ];
    }

    /**
     * Returns the container dependencies.
     * Maps factory interfaces to factories.
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [
            'factories' => [
                Decoder::class => Factory\DecoderFactory::class,
                Encoder::class => Factory\EncoderFactory::class,
                SchemaManager::class => Factory\SchemaManagerFactory::class,
            ],
        ];
    }
}